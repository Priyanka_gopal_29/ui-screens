import React from "react";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import Welcome from './components/Welcome';
import Register from './components/Register';
import BankAccountInfo from './dashboard/component1/BankAccountInfo';
import BankAccount from './dashboard/component2/BankAccount';
// import UserDetails from './redux/UserDetails';
import ChartDashboard from './dashboard/chart/ChartDashboard';



const Router = () => {
  return (
    <div>
      <BrowserRouter>
        <Switch>
          <Route exact path="/register" component={Register} />
          <Route exact path="/welcome" component={Welcome} />
          <Route exact path="/BankAccountInfo" component={BankAccountInfo} />
          <Route exact path="/BankAccount" component={BankAccount} />
           {/* <Route exact path="/UserDetails" component={UserDetails}/> */}
           <Route exact path="/ChartDashboard" component={ChartDashboard}/>
          <Redirect from="/" to="/ChartDashboard" />
        </Switch>
      </BrowserRouter>
    </div>
  );
};

export default Router;
