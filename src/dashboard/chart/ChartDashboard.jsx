import React from 'react';
import 'antd/dist/antd.css';
import {DownloadOutlined,DownOutlined,MenuUnfoldOutlined,CompressOutlined,ExpandOutlined,RiseOutlined,BarChartOutlined } from '@ant-design/icons';
import { Row, Col,Button,Card,Dropdown, Menu ,Radio,DatePicker,Space,RangePicker,Divider} from 'antd';
import prof3 from '../../assets/prof3.png';
import konnektivelogo from '../../assets/konnektivelogo.png';
import dashboardicon from '../../assets/dashboardicon.png';
import profileicon from '../../assets/profileicon.png';
import logout from '../../assets/logout.png';
import LinChart from "./LinChart";
import "./styles.css";


const ChartDashboard =()=>{
    const { RangePicker } = DatePicker;
    return(
          <Row className="mainContainer">
          {/* left */}
            <Col span={4} className="leftPannel">
               <div><img alt="konnektivelogo" src={konnektivelogo} className="konnektivelogo"></img></div>
            <Row className="user">
              <Col><img alt="dashboardicon" src={dashboardicon} className="dashboardicon"></img></Col>
              <Col className="dashboard">Dashboard</Col>
            </Row>
             <Row className="user">
              <Col><img alt="profileicon" src={profileicon} className="profileicon"></img></Col>
              <Col className="dashboard">Profile</Col>
            </Row>
            <Row className="user">
              <Col><img alt="profileicon" src={profileicon} className="profileicon"></img></Col>
              <Col className="dashboard">Pending Requests</Col>
            </Row>
            <Row className="user">
              <Col><img alt="profileicon" src={profileicon} className="profileicon"></img></Col>
              <Col className="dashboard">Affilicate Management</Col>
            </Row>
            <Row className="user">
              <Col><img alt="profileicon" src={profileicon} className="profileicon"></img></Col>
              <Col className="dashboard">Commission Summary</Col>
            </Row>
            <div><img alt="logout" src={logout} className="log"></img></div>
            <div><button className="logout-button">Log out</button></div>
            <div className="version">version 0.1</div>
            </Col>
            {/* right */}
            <Col span={20} className="rightPannel">
            {/* header */}
             <Row className="account">
              <Col span={16} className="usage1">Dashboard /Performance Report</Col>
                <Col span={8}>
                <Row>
                <Col>
                  <Button type="primary" shape="round" icon={<DownloadOutlined />}>INVITE AFFILILATE</Button>
                  </Col>
                  <Col>
                  <div className="side">
                      <img alt="prof3" src={prof3} className="prof3"></img>
                      <Button type="link" className="jonnas">Michael Jonnas</Button>
                  </div>
                  </Col>
                  </Row>
                </Col>
            </Row>
            {/* dashboard */}
            {/* linechart */}
              <Card className="linecard">
                <Row style={{width:"100%"}}> 
                    <Col span={3}>
                        <Dropdown>
                            <a className="ant-dropdown-link">
                            Hover me <DownOutlined />
                            </a>
                       </Dropdown>
                    </Col>
                    <Col span={9}>
                        <Row>
                          <Col><Radio>Campaign1</Radio></Col>
                          <Col><Radio>Campaign2</Radio></Col>
                          <Col><Radio>Campaign3</Radio></Col>
                          <Col><Radio>Campaign4</Radio></Col>
                        </Row>
                    </Col>
                    <Col span={8}>
                    <Row>
                       <Col span={4}><Button icon={<BarChartOutlined />}/></Col>
                       <Col span={4}><Button  type="primary" icon={<RiseOutlined />}/></Col>
                        <Col span={14}>
                            <Space direction="vertical">
                                <RangePicker />
                            </Space>
                        </Col>
                     
                    </Row>
                    </Col>
                     <Col span={4}>
                      <Dropdown>
                        <Button>
                            Actions <DownOutlined />
                        </Button>
                      </Dropdown>
                     </Col>
                    </Row>
                  
              <LinChart />
              </Card>
              {/* tablecard */}
              <Card className="tablecard">
                <Row style={{width:"100%"}}> 
                    <Col span={15} style={{fontSize:"20px"}}>
                        Performance Report
                    </Col>
                    <Col>
                        <Row>
                          <Col span={4}><Button type="primary" icon={<CompressOutlined/>}/></Col>
                          <Col span={4}><Button icon={<ExpandOutlined />}/></Col>
                          <Col span={13}> <Button type="primary" icon={<DownloadOutlined />} >Export to GSV</Button></Col>
                          <Col span={3}><Button icon={<MenuUnfoldOutlined />}>Sortby</Button></Col>
                        </Row>
                    </Col>
                </Row>

                    <Row style={{fontWeight:"bold",marginTop:'1%'}}>
                        <Col span={1} order={9}>SALES
                        </Col>
                        <Col span={2} order={8}>STEP COMM1
                        </Col>
                        <Col span={3} order={7}>CONVERSION%
                        </Col>
                        <Col span={3} order={6}>SALES REVEN
                        </Col>
                        <Col span={3} order={5}>DECLINES
                        </Col>
                        <Col span={3} order={4}>PARTIAL%
                        </Col>
                        <Col span={3} order={3}>PARTIAL
                        </Col>
                        <Col span={3} order={2}>CPU COMM
                        </Col>
                        <Col span={3} order={1}>CLICKS
                        </Col>
                    </Row>
                    <Row style={{backgroundColor:"#e9f3fa",height:"3rem",marginTop:"1%"}}>
                        <Col span={1} order={9}>SALES
                        </Col>
                        <Col span={2} order={8}>STEP COMM1
                        </Col>
                        <Col span={3} order={7}>CONVERSION%
                        </Col>
                        <Col span={3} order={6}>SALES REVEN
                        </Col>
                        <Col span={3} order={5}>DECLINES
                        </Col>
                        <Col span={3} order={4}>PARTIAL%
                        </Col>
                        <Col span={3} order={3}>PARTIAL
                        </Col>
                        <Col span={3} order={2}>CPU COMM
                        </Col>
                        <Col span={3} order={1}>CLICKS
                        </Col>
                    </Row>
                     <Row style={{height:"3rem"}}>
                        <Col span={1} order={9}>SALES
                        </Col>
                        <Col span={2} order={8}>STEP COMM1
                        </Col>
                        <Col span={3} order={7}>CONVERSION%
                        </Col>
                        <Col span={3} order={6}>SALES REVEN
                        </Col>
                        <Col span={3} order={5}>DECLINES
                        </Col>
                        <Col span={3} order={4}>PARTIAL%
                        </Col>
                        <Col span={3} order={3}>PARTIAL
                        </Col>
                        <Col span={3} order={2}>CPU COMM
                        </Col>
                        <Col span={3} order={1}>CLICKS
                        </Col>
                    </Row>
                     <Row style={{backgroundColor:"#e9f3fa",height:"3rem"}}>
                        <Col span={1} order={9}>SALES
                        </Col>
                        <Col span={2} order={8}>STEP COMM1
                        </Col>
                        <Col span={3} order={7}>CONVERSION%
                        </Col>
                        <Col span={3} order={6}>SALES REVEN
                        </Col>
                        <Col span={3} order={5}>DECLINES
                        </Col>
                        <Col span={3} order={4}>PARTIAL%
                        </Col>
                        <Col span={3} order={3}>PARTIAL
                        </Col>
                        <Col span={3} order={2}>CPU COMM
                        </Col>
                        <Col span={3} order={1}>CLICKS
                        </Col>
                    </Row>
                     <Row style={{height:"3rem"}}>
                        <Col span={1} order={9}>SALES
                        </Col>
                        <Col span={2} order={8}>STEP COMM1
                        </Col>
                        <Col span={3} order={7}>CONVERSION%
                        </Col>
                        <Col span={3} order={6}>SALES REVEN
                        </Col>
                        <Col span={3} order={5}>DECLINES
                        </Col>
                        <Col span={3} order={4}>PARTIAL%
                        </Col>
                        <Col span={3} order={3}>PARTIAL
                        </Col>
                        <Col span={3} order={2}>CPU COMM
                        </Col>
                        <Col span={3} order={1}>CLICKS
                        </Col>
                    </Row>
                      <Row style={{height:"2rem",backgroundColor:"#e9f3fa"}}>
                        <Col span={1} order={9}>SALES
                        </Col>
                        <Col span={2} order={8}>STEP COMM1
                        </Col>
                        <Col span={3} order={7}>CONVERSION%
                        </Col>
                        <Col span={3} order={6}>SALES REVEN
                        </Col>
                        <Col span={3} order={5}>DECLINES
                        </Col>
                        <Col span={3} order={4}>PARTIAL%
                        </Col>
                        <Col span={3} order={3}>PARTIAL
                        </Col>
                        <Col span={3} order={2}>CPU COMM
                        </Col>
                        <Col span={3} order={1}>CLICKS
                        </Col>
                    </Row>
              </Card>
            </Col>
          </Row>
    )
}
export default ChartDashboard;