import React from 'react';
import {Line} from 'react-chartjs-2';

const data = {
  labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
  datasets: [
    {
      label: 'My First dataset',
      fill: false,
      lineTension: 0,
      lineWidth: 0,
     scaleShowGridLines: false,
      backgroundColor: 'rgba(75,192,192,0.4)',
      borderColor: 'rgba(75,192,192,1)',
      borderCapStyle: 'butt',
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: 'rgba(75,192,192,1)',
      pointBackgroundColor: '#fff',
      pointBorderWidth: 1,
      pointHoverRadius: 5,
      pointHoverBackgroundColor: 'rgba(75,192,192,1)',
      pointHoverBorderColor: 'rgba(220,220,220,1)',
      pointHoverBorderWidth: 2,
      pointRadius: 1,
      pointHitRadius: 10,
      data: [65, 59, 80, 81, 56, 55, 40]
    }
  ]
};
const legend = {
  display: true,
  position: "bottom",
  labels: {
    fontColor: "#323130",
    fontSize: 14
  }
};

const options = {
  title: {
    display: false,
    text: "Chart Title"
  },
  scales: {
      xAxes: [
        {
          gridLines: {
            display: false,
          },
        },
      ],
    yAxes: [{
         gridLines: {
            display: true,
          },
        ticks: {
          suggestedMin: 0,
          suggestedMax: 0
        }
      }
    ]
  }
};
const LinChart =()=>{
    return (
      <div >
        <Line data={data} height="60px" legend={legend} options={options} />
      </div>
    );
}
export default LinChart;