import React from 'react';
import 'antd/dist/antd.css';
import {DeleteOutlined,PlusOutlined } from '@ant-design/icons';
import { Row, Col,Input ,Form,Button,Radio,Select,Collapse  } from 'antd';
import konnektivelogo from '../../assets/konnektivelogo.png';
import dashboardicon from '../../assets/dashboardicon.png';
import profileicon from '../../assets/profileicon.png';
import logout from '../../assets/logout.png';
import prof3 from '../../assets/prof3.png';
import prof2 from '../../assets/prof2.png';
import Oval from '../../assets/Oval.png';

import "./styles.css"


const BankAccountInfo =()=>{
  const { Option } = Select;
  const { Panel } = Collapse;
    return(
       <Row className="mainContainer">

       {/* Main-leftPannel */}
         <Col span={4} className="leftPannel">
           <div><img alt="konnektivelogo" src={konnektivelogo} className="konnektivelogo"></img></div>
            <Row className="user">
              <Col><img alt="dashboardicon" src={dashboardicon} className="dashboardicon"></img></Col>
              <Col className="dashboard">Dashboard</Col>
            </Row>
             <Row className="user">
              <Col><img alt="profileicon" src={profileicon} className="profileicon"></img></Col>
              <Col className="dashboard">Profile</Col>
            </Row>
            <div><img alt="logout" src={logout} className="logout"></img></div>
            <div><button className="logout-button">Log out</button></div>
            <div className="version">version 0.1</div>
         </Col>

         {/* Main-rightpannel */}
         <Col span={20} className="rightPannel">

         {/* Header */}
            <Row className="account">
              <Col span={19} className="usage1">Dashboard /Bank Account Information</Col>
                <Col span={5}>
                  <div className="side">
                      <img alt="prof3" src={prof3} className="prof3"></img>
                      <Button type="link" className="jonnas">Michael Jonnas</Button>
                  </div>
                </Col>
            </Row>

          {/* Dashboard */}
             <Row className="dashboardcard">

             {/* leftpannel */}
              <Col span={24}>
                <Row>
                  <Col span={11} className="creditcard">
                   <div className="header">
                     <Form.Item>
                        <label>Bank Name:</label>
                        <Input placeholder="input placeholder"/>
                      </Form.Item>
                      <Form.Item >
                        <label>Account nick Name:</label>
                        <Input placeholder="input placeholder" />
                      </Form.Item>
                      <Form.Item>
                       <label>Routing Number:</label>
                        <Input.Password placeholder="input placeholder" />
                      </Form.Item>
                      <Form.Item>
                        <label>Confirm Routing Number:</label>
                        <Input.Password placeholder="input placeholder" />
                      </Form.Item>
                      <Form.Item>
                        <label>Account Number:</label>
                        <Input.Password placeholder="input placeholder" />
                      </Form.Item>
                      <Form.Item>
                        <label>Confirm Account Number:</label>
                        <Input.Password placeholder="input placeholder" />
                      </Form.Item>
                     <Form.Item name="Acc.Type" label="Account Type" >
                       <Radio.Group className="radiobutton">
                            <Radio value={1}>Checking</Radio>
                            <Radio value={2}>Saving</Radio>
                        </Radio.Group>
                    </Form.Item>
                    <Row className="cancelUpdate">
                       <Col span={12}><Button type="primary" ghost className="cols">Cancel</Button></Col>
                       <Col span={12}><Button type="primary" className="cols">Add Bank</Button></Col>
                    </Row>
                    </div>
                 </Col>

                 {/* center */}
                 <Col span={1}><div className="vertical"></div></Col>

                 {/* rightpannel */}
                  <Col span={10} className="inactive">
                     <div className="usage">Inactive Accounts</div>
                     <Row className="usage">
                       <Col span={21}>
                        <Collapse >
                         <Panel className="collapse" showArrow={false} header="Bank of America   xxxx xxxx xxx99" key={1}>
                              <Form.Item label="Bank Name">
                                <Input placeholder="input placeholder" />
                              </Form.Item>
                              <Form.Item label="Acc nick Name">
                                <Input placeholder="input placeholder" />
                              </Form.Item>
                              <Form.Item label="Routing No:">
                                <Input.Password placeholder="password" />
                              </Form.Item>
                              <Form.Item label="Acc.Number">
                                <Input.Password placeholder="input placeholder" />
                              </Form.Item>
                              <Form.Item name="Acc.Type" label="Acc.Type">
                               <Select
                                placeholder="Select a option and "
                                allowClear
                               >
                                <Option value="male">male</Option>
                                <Option value="female">female</Option>
                                <Option value="other">other</Option>
                              </Select>
                            </Form.Item>
                            <Button type="primary" className="activate">Activate</Button>
                         </Panel>
                        </Collapse>
                      </Col>
                      <Col span={3} ><Button  className="delete" icon={<DeleteOutlined className="delete"/>}/></Col>
                     </Row>

                     {/* 2 collapse */}
                       <Row className="usage">
                       <Col span={21}>
                        <Collapse defaultActiveKey={['1']}>
                         <Panel className="collapse" showArrow={false} header="Bank of America   xxxx xxxx xxx99">
                              <Form.Item label="Bank Name">
                                <Input placeholder="input placeholder" />
                              </Form.Item>
                              <Form.Item label="Acc nick Name">
                                <Input placeholder="input placeholder" />
                              </Form.Item>
                              <Form.Item label="Routing No:">
                                <Input.Password placeholder="password" />
                              </Form.Item>
                              <Form.Item label="Acc.Number">
                                <Input.Password placeholder="input placeholder" />
                              </Form.Item>
                              <Form.Item name="Acc.Type" label="Acc.Type">
                               <Select
                                placeholder="Select a option and "
                                allowClear
                               >
                                <Option value="male">male</Option>
                                <Option value="female">female</Option>
                                <Option value="other">other</Option>
                              </Select>
                            </Form.Item>
                            <Button type="primary">Activate</Button>
                         </Panel>
                        </Collapse>
                      </Col>
                      <Col span={3} ><Button  className="delete" icon={<DeleteOutlined className="delete"/>}/></Col>
                     </Row>

                     {/* 3 collapse */}
                    <Row className="usage">
                       <Col span={21}>
                        <Collapse defaultActiveKey={['1']}>
                         <Panel className="collapse" showArrow={false} header="Bank of America   xxxx xxxx xxx99">
                              <Form.Item label="Bank Name">
                                <Input placeholder="input placeholder" />
                              </Form.Item>
                              <Form.Item label="Acc nick Name">
                                <Input placeholder="input placeholder" />
                              </Form.Item>
                              <Form.Item label="Routing No:">
                                <Input.Password placeholder="password" />
                              </Form.Item>
                              <Form.Item label="Acc.Number">
                                <Input.Password placeholder="input placeholder" />
                              </Form.Item>
                              <Form.Item name="Acc.Type" label="Acc.Type">
                               <Select
                                placeholder="Select a option and "
                                allowClear
                               >
                                <Option value="male">male</Option>
                                <Option value="female">female</Option>
                                <Option value="other">other</Option>
                              </Select>
                            </Form.Item>
                            <Button type="primary">Activate</Button>
                         </Panel>
                        </Collapse>
                      </Col>
                      <Col span={3} ><Button  className="delete" icon={<DeleteOutlined className="delete"/>}/></Col>
                     </Row>

                      {/* 3 collapse */}
                    <Row className="usage">
                       <Col span={21}>
                        <Collapse defaultActiveKey={['1']}>
                         <Panel className="collapse" showArrow={false} header="Bank of America   xxxx xxxx xxx99">
                              <Form.Item label="Bank Name">
                                <Input placeholder="input placeholder" />
                              </Form.Item>
                              <Form.Item label="Acc nick Name">
                                <Input placeholder="input placeholder" />
                              </Form.Item>
                              <Form.Item label="Routing No:">
                                <Input.Password placeholder="password" />
                              </Form.Item>
                              <Form.Item label="Acc.Number">
                                <Input.Password placeholder="input placeholder" />
                              </Form.Item>
                              <Form.Item name="Acc.Type" label="Acc.Type">
                               <Select
                                placeholder="Select a option and "
                                allowClear
                               >
                                <Option value="male">male</Option>
                                <Option value="female">female</Option>
                                <Option value="other">other</Option>
                              </Select>
                            </Form.Item>
                            <Button type="primary">Activate</Button>
                         </Panel>
                        </Collapse>
                      </Col>
                      <Col span={3} ><Button  className="delete" icon={<DeleteOutlined className="delete"/>}/></Col>
                     </Row>
                     
                      {/* 3 collapse */}
                    <Row className="usage">
                       <Col span={21}>
                        <Collapse defaultActiveKey={['1']}>
                         <Panel className="collapse" showArrow={false} header="Bank of America   xxxx xxxx xxx99">
                              <Form.Item label="Bank Name">
                                <Input placeholder="input placeholder" />
                              </Form.Item>
                              <Form.Item label="Acc nick Name">
                                <Input placeholder="input placeholder" />
                              </Form.Item>
                              <Form.Item label="Routing No:">
                                <Input.Password placeholder="password" />
                              </Form.Item>
                              <Form.Item label="Acc.Number">
                                <Input.Password placeholder="input placeholder" />
                              </Form.Item>
                              <Form.Item name="Acc.Type" label="Acc.Type">
                               <Select
                                placeholder="Select a option and "
                                allowClear
                               >
                                <Option value="male">male</Option>
                                <Option value="female">female</Option>
                                <Option value="other">other</Option>
                              </Select>
                            </Form.Item>
                            <Button type="primary">Activate</Button>
                         </Panel>
                        </Collapse>
                      </Col>
                      <Col span={3} ><Button  className="delete" icon={<DeleteOutlined className="delete"/>}/></Col>
                     </Row>
                     
                      {/* 3 collapse */}
                    <Row className="usage">
                       <Col span={21}>
                        <Collapse defaultActiveKey={['1']}>
                         <Panel className="collapse" showArrow={false} header="Bank of America   xxxx xxxx xxx99">
                              <Form.Item label="Bank Name">
                                <Input placeholder="input placeholder" />
                              </Form.Item>
                              <Form.Item label="Acc nick Name">
                                <Input placeholder="input placeholder" />
                              </Form.Item>
                              <Form.Item label="Routing No:">
                                <Input.Password placeholder="password" />
                              </Form.Item>
                              <Form.Item label="Acc.Number">
                                <Input.Password placeholder="input placeholder" />
                              </Form.Item>
                              <Form.Item name="Acc.Type" label="Acc.Type">
                               <Select
                                placeholder="Select a option and "
                                allowClear
                               >
                                <Option value="male">male</Option>
                                <Option value="female">female</Option>
                                <Option value="other">other</Option>
                              </Select>
                            </Form.Item>
                            <Button type="primary">Activate</Button>
                         </Panel>
                        </Collapse>
                      </Col>
                      <Col span={3} ><Button  className="delete" icon={<DeleteOutlined className="delete"/>}/></Col>
                     </Row>
                      {/* 3 collapse */}
                    <Row className="usage">
                       <Col span={21}>
                        <Collapse defaultActiveKey={['1']}>
                         <Panel className="collapse" showArrow={false} header="Bank of America   xxxx xxxx xxx99">
                              <Form.Item label="Bank Name">
                                <Input placeholder="input placeholder" />
                              </Form.Item>
                              <Form.Item label="Acc nick Name">
                                <Input placeholder="input placeholder" />
                              </Form.Item>
                              <Form.Item label="Routing No:">
                                <Input.Password placeholder="password" />
                              </Form.Item>
                              <Form.Item label="Acc.Number">
                                <Input.Password placeholder="input placeholder" />
                              </Form.Item>
                              <Form.Item name="Acc.Type" label="Acc.Type">
                               <Select
                                placeholder="Select a option and "
                                allowClear
                               >
                                <Option value="male">male</Option>
                                <Option value="female">female</Option>
                                <Option value="other">other</Option>
                              </Select>
                            </Form.Item>
                            <Button type="primary">Activate</Button>
                         </Panel>
                        </Collapse>
                      </Col>
                      <Col span={3} ><Button  className="delete" icon={<DeleteOutlined className="delete"/>}/></Col>
                     </Row>
                      {/* 3 collapse */}
                    <Row className="usage">
                       <Col span={21}>
                        <Collapse defaultActiveKey={['1']}>
                         <Panel className="collapse" showArrow={false} header="Bank of America   xxxx xxxx xxx99">
                              <Form.Item label="Bank Name">
                                <Input placeholder="input placeholder" />
                              </Form.Item>
                              <Form.Item label="Acc nick Name">
                                <Input placeholder="input placeholder" />
                              </Form.Item>
                              <Form.Item label="Routing No:">
                                <Input.Password placeholder="password" />
                              </Form.Item>
                              <Form.Item label="Acc.Number">
                                <Input.Password placeholder="input placeholder" />
                              </Form.Item>
                              <Form.Item name="Acc.Type" label="Acc.Type">
                               <Select
                                placeholder="Select a option and "
                                allowClear
                               >
                                <Option value="male">male</Option>
                                <Option value="female">female</Option>
                                <Option value="other">other</Option>
                              </Select>
                            </Form.Item>
                            <Button type="primary">Activate</Button>
                         </Panel>
                        </Collapse>
                      </Col>
                      <Col span={3} ><Button  className="delete" icon={<DeleteOutlined className="delete"/>}/></Col>
                     </Row>
                     
                      {/* 3 collapse */}
                    <Row className="usage">
                       <Col span={21}>
                        <Collapse defaultActiveKey={['1']}>
                         <Panel className="collapse" showArrow={false} header="Bank of America   xxxx xxxx xxx99">
                              <Form.Item label="Bank Name">
                                <Input placeholder="input placeholder" />
                              </Form.Item>
                              <Form.Item label="Acc nick Name">
                                <Input placeholder="input placeholder" />
                              </Form.Item>
                              <Form.Item label="Routing No:">
                                <Input.Password placeholder="password" />
                              </Form.Item>
                              <Form.Item label="Acc.Number">
                                <Input.Password placeholder="input placeholder" />
                              </Form.Item>
                              <Form.Item name="Acc.Type" label="Acc.Type">
                               <Select
                                placeholder="Select a option and "
                                allowClear
                               >
                                <Option value="male">male</Option>
                                <Option value="female">female</Option>
                                <Option value="other">other</Option>
                              </Select>
                            </Form.Item>
                            <Button type="primary">Activate</Button>
                         </Panel>
                        </Collapse>
                      </Col>
                      <Col span={3} ><Button  className="delete" icon={<DeleteOutlined className="delete"/>}/></Col>
                     </Row>
                     
                     
                     
                  </Col>
                </Row>
              </Col>
            </Row>
         </Col>
       </Row>
    )
}
export default BankAccountInfo;