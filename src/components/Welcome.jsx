import React from 'react';
import 'antd/dist/antd.css';
import { Row, Col,Input ,Checkbox,Button } from 'antd';
import welcome from '../assets/welcome.png';
import logo_sm from '../assets/logo_sm.png';
import recaptcha from '../assets/recaptcha.png';
import '../styles/welcome.css'

const Welcome = ()=>{
    return(
        <Row>
          <Col flex="1 1 500px"><img alt="welcome" src={welcome} className="welcome"></img></Col>
          <Col flex="1 1 100px" className="left">
            <div>
                <div><img alt="logo_sm" src={logo_sm} className="logo_sm"></img></div>
                <div className="signup">
                  <Row>
                      <Col>Sign</Col>
                      <Col className="up">In</Col>
                  </Row>
                </div>
                <div><Input placeholder="Username or email id" className="inputfield" bordered={false} /></div>
                <div ><Input.Password placeholder="input password" className="inputpasswordfield" bordered={false}/></div>
                <div className="forget">Forget password ?</div>
                <div className="robotContainer">
                  <Row className="checkboxfield">
                      <Col span={18} ><Checkbox>I'm not a robot</Checkbox></Col>
                      <Col span={5} ><img alt="recaptcha" src={recaptcha} className="recaptcha"></img></Col>
                  </Row>
                </div>
                <div><Button type="primary" shape="round" className="signupButton">signup</Button></div>
                <Row className="footer">
                      <Col className="first">First Time Here?</Col>
                      <Col className="click"><Button type="link" size="large">Click here to signup</Button></Col>
                  </Row>
            </div>
          </Col>
        </Row>       
    )
}

export default Welcome;