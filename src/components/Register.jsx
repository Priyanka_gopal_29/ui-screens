import React,{useState} from 'react';
import 'antd/dist/antd.css';
import { Row, Col,Input ,Checkbox,Button,Radio ,InputNumber } from 'antd';
import register from '../assets/register.png';
import logo_sm from '../assets/logo_sm.png';
import recaptcha from '../assets/recaptcha.png';
import '../styles/register.css'

const Welcome = ()=>{
    return(
        <Row>
          <Col flex="1 1 500px"><img alt="register" src={register} className="register"></img></Col>
          <Col flex="1 1 100px" className="left">
            <div>
                <div><img alt="logo_sm" src={logo_sm} className="logo_sm"></img></div>
                <div className="signup">
                  <Row>
                      <Col>Register</Col>
                      <Col className="here">Here</Col>
                  </Row>
                </div>
                <Radio.Group  className="radioSwitch">
                    <Radio value={1} >Affiliate</Radio>
                    <Radio value={2}>Merchart</Radio>
                </Radio.Group>
                
                <div><Input placeholder="Username " className="inputfield" bordered={false}/></div>
                <Row className="split">
                   <Col span={11}><Input placeholder="Username " className="inputfield" bordered={false} /></Col>
                    <Col span={11}><Input placeholder="Username " className="inputfield" bordered={false} /></Col>
                </Row>
                <div><Input placeholder="User Number" className="inputfield" bordered={false} /></div>
                <Row className="about">
                   <Col>Tell us about YourSelf :</Col>
                    <Col className="here">(Optional)</Col>
                </Row>
                <Row>
                   <Col><Input placeholder="Username" className="inputfield" bordered={false} /></Col>
                    <Col><Input placeholder="Username" className="inputfield" bordered={false} /></Col>
                </Row>
                <div><Button shape="round" className="signupButton">Request Access</Button></div>
                 <Row className="footer">
                      <Col className="first">By logging in, you agree to the</Col>
                      <Col className="click"><Button className="terms" type="link" size="medium">terms of service</Button></Col>
                </Row>
            </div>
          </Col>
        </Row>       
    )
}

export default Welcome;